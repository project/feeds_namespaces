-- SUMMARY --

The Feeds namespaces module provides an extensible RSS parser for Feeds.
Plugin modules provide support for different namespace extensions.

Project page:
  http://drupal.org/project/feeds_namespaces

Issue queue:
  http://drupal.org/project/issues/1239718

-- REQUIREMENTS --

 * Feeds (http://drupal.org/project/feeds)

-- INSTALLATION --

Install just like any other module.

See:
  http://drupal.org/documentation/install/modules-themes/modules-7

You can install plugin modules that provide support for additional namespaces.

-- CONFIGURATION --

Select FeedsNamespacesParser as a parser in the Feeds UI. Then define your
mappings as usual.

-- DEVELOP --

Read the developer documentation: http://drupal.org/node/1243954

-- CONTACT --

Currently maintained by Niklas Fiekas:
  http://drupal.org/user/1089248
  niklas.fiekas@googlemail.com
