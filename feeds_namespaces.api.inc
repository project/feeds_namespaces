<?php

/**
 * @file
 * Hooks provided by the Feeds namespaces module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Declares support for namespaces.
 *
 * The array format, including how to define mapping sources, is documented
 * at http://drupal.org/node/1243954.
 *
 * This hook can be implemented in a module.feeds_namespaces.inc file. The file
 * will be automatically included when needed.
 *
 * @return
 *   Associative array of namespaces and their settings.
 */
function hook_feeds_namespaces() {
  $ns['http://example.com/example-namespace-url'] = array(
    'name' => t('The name of the namespace'),
    'description' => t('A longer description of the namespace.'),
    'sources' => array(
      // Available mapping sources
    ),
  );

  return $ns;
}

/**
 * Alters namespaces that are already declared.
 *
 * This hook can be implemented in a module.feeds_namespaces.inc file. The file
 * will be automatically included when needed.
 *
 * @param $ns
 *   An associative array of the declared namespaces.
 *   See hook_feeds_namespaces() for the structure.
 *
 * @see hook_feeds_namespaces()
 */
function hook_feeds_namespaces_alter(&$ns) {
  $ns['http://example.com/example-namespace-url']['name'] = t('Altered name');
}

/**
 * @} End of "addtogroup hooks".
 */
