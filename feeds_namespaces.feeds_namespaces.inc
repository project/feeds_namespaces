<?php

/**
 * @file
 * Provide support for the core namespace of RSS.
 */

/**
 * Implements hook_feeds_namespaces().
 */
function feeds_namespaces_feeds_namespaces() {
  $ns[FEEDS_NAMESPACES_RSS] = array(
    'name' => t('RSS 2.0'),
    'description' => t('The core RSS format'),
    'sources' => array(
      'title' => array(
        'name' => t('Title'),
        'description' => t('Title of the item.'),
      ),
      'link' => array(
        'name' => t('Item URL (link)'),
        'description' => t('The URL of the item.'),
      ),
      'description' => array(
        'name' => t('Description'),
        'description' => t('The item synopsis.'),
      ),
      'author' => array(
        'name' => t('Author name'),
        'description' => t('The author of the item.'),
        'default value callback' => 'feeds_namespaces_rss_author',
      ),
      'category' => array(
        'name' => t('Category'),
        'description' => t('One or more categories of the item.'),
        'multiple' => TRUE,
      ),
      'comments' => array(
        'name' => t('Comments URL'),
        'description' => t('URL of a page for comments relating to the item.'),
      ),
      'enclosure' => array(
        'name' => t('Enclosure'),
        'description' => t('Media included with the item'),
        'attribute' => 'url',
      ),
      'guid' => array(
        'name' => t('Item GUID'),
        'description' => t('Global Unique Identifier of the feed item.'),
      ),
      'pubDate' => array(
        'name' => t('Published date'),
        'description' => t('Published date as UNIX time GMT of the item.'),
        'rewriters' => array('strtotime'),
      ),
    ),
  );

  return $ns;
}

/**
 * Get the author of a feed.
 *
 * @return
 *   The author as a string or NULL if no author was found.
 */
function feeds_namespaces_rss_author($namespace, $sourcename, $feed, $entry, $source) {
  if (isset($feed->channel->author)) {
    return (string) $feed->channel->author;
  }
}
