<?php

/**
 * @file
 * The FeedsParser child class.
 */

class FeedsNamespacesParser extends FeedsParser {

  public function parse(FeedsSource $source, FeedsFetcherResult $fetched) {
    // Parse the feed using SimpleXML.
    // See FeedsYoutubeParser::parse from feeds_youtube.
    $content = $fetched->getRaw();
    if (!defined('LIBXML_VERSION') || (version_compare(phpversion(), '5.1.0', '<'))) {
      @$feed = simplexml_load_string($content, NULL);
    }
    else {
      @$feed = simplexml_load_string($content, NULL, LIBXML_NOERROR | LIBXML_NOWARNING | LIBXML_NOCDATA);
    }
    if (!$feed) {
      throw new Exception(t('FeedsNamespacesParser: Malformed XML source.'));
    }

    // Get configuration.
    $namespaces = feeds_namespaces_get();
    $importer_config = feeds_importer($this->id)->getConfig();


    $result = new FeedsParserResult();

    foreach ($feed->channel->item as $entry) {
      $item = array();

      // Process all defined mappings.
      foreach ($importer_config['processor']['config']['mappings'] as $mapping) {
        // Split mapping name into namespace and source name.
        list($namespace, $source_name) = explode('@@', $mapping['source'], 2);

        // Skip if the namespace is not supported.
        if (!isset($namespaces[$namespace])) {
          continue;
        }

        // Execute the callback.
        $value = call_user_func(
          $namespaces[$namespace]['sources'][$source_name]['callback'],
          $namespace,
          $source_name,
          $feed,
          $entry,
          $namespaces[$namespace]['sources'][$source_name]);

        if ($value === NULL) {
          // Use the default value.
          $value = call_user_func(
            $namespaces[$namespace]['sources'][$source_name]['default value callback'],
            $namespace,
            $source_name,
            $feed,
            $entry,
            $namespaces[$namespace]['sources'][$source_name]);
        }
        else {
          // Execute the rewriters.
          foreach ($namespaces[$namespace]['sources'][$source_name]['rewriters'] as $rewriter) {
            $value = call_user_func($rewriter, $value);
          }
        }

        if ($value === FALSE) {
          $value = NULL;
        }

        $item[$mapping['source']] = $value;
      }

      $result->items[] = $item;
    }

    return $result;
  }

  public function getMappingSources() {
    $namespaces = feeds_namespaces_get();

    $sources = parent::getMappingSources();

    foreach ($namespaces as $namespace_name => $namespace) {
      foreach ($namespace['sources'] as $source_name => $source) {
        $sources[$namespace_name . '@@' . $source_name] = array( 
          'name' => $namespace['name'] . ': ' . $source['name'],
          'description' => $source['description'],
        );
      }
    }

    return $sources;
  }
}
